import React, { useEffect } from 'react'
import Register from './pages/Register'
import Login from './pages/Login'
import Posts from './pages/Posts'
import Photos from './pages/Photos'
import { 
  BrowserRouter,
  Routes,
  Route,
  useNavigate
} from 'react-router-dom'


const App = () => {
  
  return(
     <Routes>
      <Route path="/" element={<Login />} />
      <Route path="/login" element={<Login />} />
      <Route path="/register" element={<Register />} />
      <Route path="/posts" element={<Posts />} />
      <Route path="/photos" element={<Photos />} />
    </Routes>
  )
}

export default App