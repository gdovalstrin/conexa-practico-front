import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import '../App.css'
import axios from 'axios'

function Login() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const navigate = useNavigate()
  var changeRoute = () => {
    navigate('/register', { replace: true })
  }

  useEffect(() => {
    if (localStorage.token) localStorage.removeItem('token')
  }, [])

  async function loginUser(event) {
    event.preventDefault()
    const response = await axios.post('https://practico-conexa.herokuapp.com/api/login', {
      email,
      password
    },
    {
      headers: {
        'Content-Type': 'application/json', 
      }
    })
    const data = await response.data
    /*
    const response = await fetch('https://practico-conexa.herokuapp.com/api/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json', 
      },
      body: JSON.stringify({
        email,
        password
      }),
    })
    */

    //const data = await response.json()

    if (data.user) {
      localStorage.setItem('token', data.user)
      alert('Login successful!')
      navigate('/posts')
    } else {
      alert('Check name or password')
    }

    console.log(data)
  }

  return (
    <div className="App">
        <h1>Login</h1>
        <form onSubmit={loginUser} style={{alignItems: "center"}}>
          <input 
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            type="email" 
            placeholder='Email' 
          />
          <br />
          <input 
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            type="password" 
            placeholder='Password' 
          />
          <br />
          <input type="submit" value="Login" />
        </form>
        <p className='to-link' onClick={changeRoute}>No tenes cuenta?</p>
    </div>
  );
}

export default Login;
