import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { Card, CardContent, Typography } from '@mui/material';
import { Box } from '@mui/system';
import jwt from 'jwt-decode'
import axios from 'axios'



const Posts = () => {
    const navigate = useNavigate()
    const [posts, setPosts] = useState([])

    const routeChange = () =>{ 
        let path = '/photos'; 
        navigate(path);
    }

    async function getPosts() {
        const req = await axios.get('https://practico-conexa.herokuapp.com/api/posts/', {
            headers: {
                'x-access-token': localStorage.getItem('token')
            }
        })
        /*
        const req = await fetch('http://localhost:3001/api/posts', 
        {
            headers: {
                'x-access-token': localStorage.getItem('token')
            }
        })
        */


        console.log(req)
        const data = req.data
        if (data.status === 'ok') {
            setPosts(data.posts)
        } else {
            alert(data.error)
        }
    }

    useEffect(() => {
        const token = localStorage.getItem('token')
        if (token) {
            const user = jwt(token)
            if (!user) {
                localStorage.removeItem('token')
                navigate('/login', { replace: true })
            } else {
                getPosts()
            }
        } else {
            navigate('/login', { replace: true })
        }
    }, [])

    const cardStyle = {
        display: "block",
        backgroundColor: "#FEC8D8",
        alignSelf: "center",
        marginLeft: "5%",
        marginRight: "5%",
        marginTop: "2%"
    };

    const boxStyle = {
        display: "block",
        alignSelf: "center"
    };
    
    return (
        <div>
            <button className='change-page' onClick={routeChange}>Ver pagina de fotos</button>
            <h1>Lista de posts</h1>
            <div className='container'>
            <Box style={boxStyle}>
                {
                    posts.map(post => {
                        return (
                            <Card style={cardStyle} variant="outlined" sx={{ minWidth: 275 }} key={post.id}>
                                <CardContent>
                                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                                    user ID: {post.userId}
                                    </Typography>
                                    <Typography variant="h5" component="div">
                                    {post.title}
                                    </Typography>
                                    <Typography sx={{ mb: 1.5 }} color="text.secondary">
                                    post ID: {post.id}
                                    </Typography>
                                    <Typography variant="body2">
                                    {post.body}
                                    </Typography>
                                </CardContent>
                            </Card>
                        )
                    })
                }
                </Box>
                </div>
        </div>
    )
}

export default Posts