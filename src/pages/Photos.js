import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import ReactPaginate from 'react-paginate'
import jwt from 'jwt-decode'                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
import '../App.css'

const Photos = () => {
    const navigate = useNavigate()
    const [photos, setPhotos] = useState([])

    // para paginacion
    const [currentPage, setCurrentPage] = useState(0);
    const PER_PAGE = 10
    const offset = currentPage * PER_PAGE

    console.log(photos.slice(offset, offset + PER_PAGE).map(photo => <img src={photo.url} />))


    const currentPageData = photos
        .slice(offset, offset + PER_PAGE)
        .map(photo => <img src={photo.url} alt={photo.title} height={600} width={600} key={photo.id} />)

    const pageCount = Math.ceil(photos.length / PER_PAGE);

    const routeChange = () =>{ 
        let path = '/posts'
        navigate(path)
    }

    async function getPhotos() {
        const req = await fetch('https://practico-conexa.herokuapp.com/api/photos', 
        {
            headers: {
                'x-access-token': localStorage.getItem('token')
            }
        })

        console.log(req)
        const data = await req.json()
        if (data.status === 'ok') {
            setPhotos(data.photos)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
        } else {
            alert(data.error)
        }
    }

    useEffect(() => {
        const token = localStorage.getItem('token')
        if (token) {
            const user = jwt(token)
            if (!user) {
                localStorage.removeItem('token')
                navigate('/login', { replace: true })
            } else {
                getPhotos()
            }
        } else {
            navigate('/login', { replace: true })
        }
    }, [])

    function handlePageClick({ selected: selectedPage }) {
        setCurrentPage(selectedPage);
    }

    return (
        <div>
            <button className='change-page' onClick={routeChange}>Ver pagina de posts</button>
            <h1>Lista de fotos</h1>
            {currentPageData}
            <ReactPaginate
                previousLabel={"← Previous"}
                nextLabel={"Next →"}
                pageCount={pageCount}
                onPageChange={handlePageClick}
                containerClassName={"pagination"}
                previousLinkClassName={"pagination__link"}
                nextLinkClassName={"pagination__link"}
                disabledClassName={"pagination__link--disabled"}
                activeClassName={"pagination__link--active"}
            />
        </div>
    )
    
    /*
    return (
        <div>
            <button onClick={routeChange}>Ver pagina de posts</button>
            <h1>Lista de fotos</h1>
            <ul>
                {
                    photos.map(photo => {
                        return (
                            <img src={photo.url} alt={photo.title} height={100} width={100} key={photo.id} />
                        )
                    })
                }
            </ul>
        </div>
    )
    */
}

export default Photos