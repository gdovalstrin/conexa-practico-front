import React from 'react';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import '../App.css'

function Register() {
  const navigate = useNavigate()

  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  var changeRoute = () => {
    navigate('/login')
  }

  useEffect(() => {
    if (localStorage.token) localStorage.removeItem('token')
  }, [])

  async function registerUser(event) {
    event.preventDefault()
    const response = await fetch('https://practico-conexa.herokuapp.com/api/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json', 
      },
      body: JSON.stringify({
        name,
        email,
        password
      }),
    })

    const data = await response.json()

    if (data.status === 'ok') {
      navigate('/login')
    } else {
      alert('User ya existe')
    }

    console.log(data)
  }

  return (
    <div className="App">
        <h1>Register</h1>
        <form onSubmit={registerUser}>
          <input 
            value={name}
            onChange={(e) => setName(e.target.value)}
            type="text" 
            placeholder='Name'
          />
          <br />
          <input 
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            type="email" 
            placeholder='Email' 
          />
          <br />
          <input 
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            type="password" 
            placeholder='Password' 
          />
          <br />
          <input className='submit-btn' type="submit" value="Register" />
        </form>
        <p className='to-link' onClick={changeRoute}>Ya tenes cuenta?</p>
    </div>
  );
}

export default Register;
